import jwt

from datetime import datetime, timedelta

from django.conf import settings
from django.contrib.auth.models import (
    AbstractBaseUser, BaseUserManager, PermissionsMixin
)
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserManager(BaseUserManager):
    """
    En django necesitamos definir un manager propio para un usurio personalizado.
    Debemos sobreescribir el create user
    """

    def create_user(self, name, email, password=None):
        """Creamos un usuario y retornamos name, email y password"""
        if name is None:
            raise TypeError('Users must have a name.')

        if email is None:
            raise TypeError('Users must have an email address.')

        user = self.model(name=name, email=self.normalize_email(email))
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, name, email, password):
        """
        Crear superusuario
        """
        if password is None:
            raise TypeError('Superusers must have a password.')

        user = self.create_user(name, email, password)
        user.is_superuser = True
        user.is_staff = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):

    name = models.CharField(db_index=True, max_length=255, unique=True)
    email = models.EmailField(db_index=True, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    # 'USERNAME_FIELD' hace que cuando el usuario se loguee lo haga mediante el campo 'email'
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    # Con esto indicamos a Django que use el Manager que hemos creado antes
    objects = UserManager()

    def __str__(self):
        return self.email

    @property
    def token(self):
        """
        Propiedad de token para el usuario
        """
        return self._generate_jwt_token()

    def get_full_name(self):
        """
        Esta propiedad es obligada por Django cuando cambiamos el usuario base
        """
        return self.name

    def get_short_name(self):
        """
        Propiedad obligada de django
        """
        return self.name

    def _generate_jwt_token(self):
        """
        Genera un JSON Web Token que expira a los 60 dias
        """
        dt = datetime.now() + timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=60, blank=True, null=True)
    last_name = models.CharField(max_length=60, blank=True, null=True)
    # No funciona en el contenedor de docker, tiene instalada la libreria de Pillow,
    # pero el contenedor no la reconoce TODO
    # avatar = models.ImageField(upload_to='mediafiles')

    def __str__(self):
        return self.first_name


@receiver(post_save, sender=User)
def create_related_profile(sender, instance, created, *args, **kwargs):
    # Cuando se crea un usuario nuevo, esto nos generará su profile automaticamente
    if instance and created:
        instance.profile = Profile.objects.create(user=instance, first_name=instance.name)

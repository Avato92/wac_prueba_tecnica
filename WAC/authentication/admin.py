from django.contrib import admin
from .models import User, Profile
from django.contrib.auth.models import Group


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    actions = None
    list_display = ['name', 'email', 'is_active']


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    actions = None
    list_display = ['last_name', 'first_name']


admin.site.unregister(Group)

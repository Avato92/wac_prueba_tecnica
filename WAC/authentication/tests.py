
from rest_framework.test import APITestCase
from rest_framework import status
from django.urls import reverse
from .models import User
from django.test import RequestFactory
import json


class RegistrationTestCase(APITestCase):

    def test_registration(self):
        user = {"user": {"name": "test", "email": "test@gmail.com", "password": "12345678"}}
        response = self.client.post("/api/users/", user, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(User.objects.get().name, 'test')
        self.assertEqual(User.objects.get().email, 'test@gmail.com')

    def test_bad_registration(self):
        user = {"user": {"name": "test", "email": "test@gmail.com", "password": "1234"}}
        response = self.client.post("/api/users/", user, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_bad_registration2(self):
        user = {"user": {"name": "test", "password": "12345678"}}
        response = self.client.post("/api/users/", user, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class LoginTestCase(APITestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            name='test', email='test@gmail.com', password='12345678')
        self.user.save()

    def test_login(self):
        user = {"user": {"email": "test@gmail.com", "password": "12345678"}}
        response = self.client.post("/api/users/login/", user, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.get().name, 'test')
        self.assertEqual(User.objects.get().email, 'test@gmail.com')

    def test_bad_login(self):
        user = {"user": {"email": "test@gmail.com", "password": "xsfadaf"}}
        response = self.client.post("/api/users/login/", user, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class RetrieveTestCase(APITestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            name='test', email='test@gmail.com', password='12345678')
        self.user.save()

    def test_bad_retrieve(self):
        response = self.client.get("/api/user")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_retrieve(self):
        user = {"user": {"email": "test@gmail.com", "password": "12345678"}}
        user = self.client.post("/api/users/login/", user, format="json")
        # Falta introducir el token en el header TODO
        response = self.client.get("/api/user", None, **{'Authorization': 'Token ' + user.data['token']})
        print(response)


class UpdateTestCase(APITestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
            name='test', email='test@gmail.com', password='12345678')
        self.user.save()

    def test_bad_update(self):
        response = self.client.put("/api/user")
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

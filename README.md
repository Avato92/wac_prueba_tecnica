# Prueba técnica WAC
Autor: ***Alejandro Vañó Tomás***

## Prueba a realizar:

- Gestión básica de usuario.
- Hacer login en la API
- Editar el perfil del propio usuario

## Requisitos Técnicos

- Python 3.X.X
- Django 2.X.X
- DDBB Postgres
- Variables de entorno para gestionar settings.py

# Lanzar aplicación

La aplicación está pensada para que se ejecute en **modo producción**, para ello 
lo primero que debemos hacer es clonar el repositorio.

A la aplicación se le pueden introducir archivos con variables de entorno, para ello solo
debemos mirar los archivos:
- .env.prod.example
- .env.prod.db.example

Si modificamos el nombre de los archivos y le quitamos el .example, funcionará.

Una vez clonado y solucionado lo de las variables de entorno, deberemos ejecutar los siguientes comandos:

Es posible que para utilizar estos comandos necesites ```sudo```

```docker-compose -f docker-compose.prod.yml up -d --build```

Con esto la aplicación ya estará funcionando, pero aún debemos ejecutar la migración de 
la BBDD y los archivos estáticos para que el servidor pueda leerlos.

```docker-compose -f docker-compose.prod.yml exec web python manage.py makemigrations```

```docker-compose -f docker-compose.prod.yml exec web python manage.py migrate --noinput```

**OJO** En caso de ejecutar los anteriores comandos y que no aplique cambios o de algún error, ejecutar lo siguiente:

```docker-compose -f docker-compose.prod.yml exec web python manage.py makemigrations authentication```

Y luego ya podemos seguir con el comando migrate normal.

```docker-compose -f docker-compose.prod.yml exec web python manage.py collectstatic --no-input --clear```

Con esto se habrán aplicado los cambios en la BBDD y se habrán copiado todos los archivos
estáticos para que por ejemplo el CSS del django-admin funcione.

Para acceder al django-admin deberán entrar en esta url [127.0.0.1:1337/admin](http://127.0.0.1:1337/admin/)

Si queremos crear un superusuario con el que poder entrar al django-admin deberemos ejecutar:

```docker-compose -f docker-compose.prod.yml exec web python manage.py createsuperuser```

Y seguimos los pasos para crear un superusuario en django, introduciendo el nombre, email y la 
contraseña dos veces.

A la hora de entrar en el panel de Django-admin, recordar que ahora hay que loguearse mediante el email y la contraseña,
lo normal es hacerlo mediante un nombre de usuario y la contraseña.

Una vez hecho todo, si queremos parar y borrar tanto los contenedores como sus volúmenes,
deberemos ejecutar:

```docker-compose -f docker-compose.prod.yml down -v --remove-orphans```

### TODO

Hacer un script que ejecute los comandos anteriores automaticamente

## API

Una vez desplegado podremos atacar a la API del servidor

| Petición         | Método           | Headers                        | Params                                                                      |
| :--------------- |:----------------:| :-----------------------------:| :--------------------------------------------------------------------------:|
| /api/users/      | **POST**         | Content-type: application/json | {"user": {"name":"test", "email":"test@gmail.com", "password": "12345678"}} |
| /api/users/login | **POST**         | Content-type: application/json | {"user": {"email": "test@gmail.com", "password": "12345678"}}               |
| /api/user        | **GET**          | Authorization: Token {token}   |                                                                             |
| /api/user        | **PUT**          | Authorization: Token {token}   | {"user": {"name": "jhj", "last_name": "fdfh", "first_name": "hfu"}}         |

## NOTAS

El campo de avatar no he podido introducirlo, el contenedor de docker da error al hacer el manage.py makemigrations, porque dice que Pillow no está instalado.
He entrado en el contenedor y si que está instalada, he estado indagando y no he conseguido solucionarlo, necesitaria mirarlo com más detenimiento.

## Testing

```docker-compose -f docker-compose.prod.yml exec web python manage.py test```
